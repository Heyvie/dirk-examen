-- creëer database als ze nog niet bestaat
CREATE DATABASE IF NOT EXISTS CompetitionDB;
USE CompetitionDB;

-- verwijder tabel als ze al bestaat
DROP TABLE IF EXISTS 'Game';
CREATE TABLE 'Game'(
    $Date datetime(20),
    $Time datetime(10),
    $Status TEXT,
    $HomeScore(5),
    $VisitorScore(5)
);

DROP TABLE IF EXISTS 'Liga';
CREATE TABLE 'Liga'(
    $Name NVARCHAR(20),
    $Year CHAR(4),
    $IsInPlanning BIT
);

DROP TABLE IF EXISTS 'Player';
CREATE TABLE 'Player'(
    $LastName NVARCHAR(20),
    $FirstName NVARCHAR(20),
    $Address NVARCHAR(20),
    $ShirtNumber CHAR(2)
);

DROP TABLE IF EXISTS `Team`;
CREATE TABLE 'Liga'(
    $Name NVARCHAR(20),
    $Location NVARCHAR(20),
    $Score CHAR(2)
);